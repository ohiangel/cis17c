/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Angel Chavez
 *
 * Created on April 11, 2018, 7:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>      //vector and functions
#include <algorithm>   //shuffle function
#include <string>      //string functions
#include <windows.h>   //sleep functions

using namespace std;

/*
 * 
 */

void menu();
void createDeck(vector<string>&);
int getCardVal(string);

int main(int argc, char** argv) {
    srand (time (0));
    menu();
    
    

    return 0;
}

void createDeck(vector<string> &myDeck){
    const string suit[]={"Hearts","Clubs","Spades","Diamonds"};
    const string face[]={"Ace","2","3","4","5","6","7","8","9","10","King"
            ,"Queen","Jack"};
    string temp="";
    for(int i=0;i<4;i++){
        for(int j=0;j<13;j++){
            temp=face[j]+" of "+suit[i];
            myDeck.push_back(temp);
        }
    }
}

int getCardVal(string card){
    int val=0;
    if(card[0]=='A'){
        val=10;
        
    }
    else if(card[0]=='K'||card[0]=='Q'||card[0]=='J'||card[0]=='1'){
        val=10;
    }
    else{
        val=card[0]-'0';
    }
    return val;
}

void menu(){
    cout << "Welcome to BlackJack!"<<endl
            <<"Please select from the following options"<<endl
            << "Select 1: Easy difficulty"<<endl
            <<"Select 2: Hard difficulty"<<endl
            <<"Select 3: Tutorial"<<endl
            <<"Select 4: Quit"<< endl;
    int playerTotal=1000;
    char i;
    cin>>i;
    switch(i)
    {
        case '1':
        {
            char play='y';
            int deckIt=0;
            vector<string> ezBlk;
            createDeck(ezBlk);
            cout << "Shuffling cards..."<<endl;
                random_shuffle(ezBlk.begin(),ezBlk.end());
                Sleep(3000);

            while(play=='y'||play=='Y'){
                int playerIt=0; //player iterator for cards
                int dealerIt=0;
                int playerBet;
                vector<string> playerHand;
                vector<string> dealerHand;
                cout << "Welcome to the easy mode!" << endl
                        << endl
                        << "Please place your bet!"<<endl;
                cout << "Your total:$"<< playerTotal << endl;
                cin>>playerBet;
                while(playerBet>playerTotal){
                    cout << "You do not have enough funds!"<< endl;
                    cout << "Enter new total:";
                    cin>>playerBet;
                }
                cout << "$"<<playerBet<<" is on the table!"<<endl;
    
                cout << string(50, '\n');
                playerHand.push_back(ezBlk[deckIt]);
                deckIt++;
                playerIt++;
                dealerHand.push_back(ezBlk[deckIt]);
                dealerIt++;
                deckIt++;
                playerHand.push_back(ezBlk[deckIt]);
                deckIt++;
                playerIt++;
                dealerHand.push_back(ezBlk[deckIt]);
                dealerIt++;
                deckIt++;
                int dealerVal=getCardVal(dealerHand[0])
                    +getCardVal(dealerHand[1]);
                int playerVal=getCardVal(playerHand[0])
                    +getCardVal(playerHand[1]);
                cout<<"Dealer's hand:" << endl
                        << dealerHand[0] << endl;
                cout << string(3,'\n');
                cout << "Your hand:"<< endl
                        << playerHand[0] << endl << playerHand[1] << endl
                        << "Your hand value:" << playerVal << endl;
                
                
                cout << "What you like to hit(h) or stand(s)?";
                string temp;
                cin>>temp;
                while(temp[0]!='h'&&temp[0]!='s'){
                    cout << endl << "Invalid option! Please enter H or S:";
                    cin.clear();
                    cin>>temp;
                }
                if(temp[0]=='h'||temp[0]=='H'){
                    while(temp[0]=='h'||temp[0]=='H'){
                        playerHand.push_back(ezBlk[deckIt]);
                        playerIt++;
                        deckIt++;
                        cout<<"Your card is " << playerHand[playerIt-1] <<endl;
                        playerVal+=getCardVal(playerHand[playerIt-1]);
                        cout<<"Your cards value is:"<<playerVal<<endl;
                        cout<<"Your cards are:";
                        
                        
                        for(int i=0;i<playerIt;i++){
                            cout <<playerHand[i]<<",";
                        }
                        
                        if(playerVal==21){
                            cout <<endl<<"Blackjack!"<< endl;
                            playerTotal+=(playerBet*2);
                            break;
                        }
                        if(playerVal>21){
                            cout <<endl<< "You bust!"<<endl;
                            playerTotal=playerTotal-playerBet;
                            cout<<"You lost:$"<<playerBet<<endl
                                    <<"Your total is:$"<<playerTotal<<endl;
                            break;
                        }
                        if(playerVal<21){
                            cout <<endl <<"Would you like to hit[h] "
                                    "or stand[s]?";
                            cin>>temp[0];
                        }
                        
                    }
                }
                if(temp[0]=='s'||temp[0]=='S'){
                    cout << string(50, '\n');
                    cout << "You chose to stand!"<<endl;
                    while(dealerVal<=19){
                        dealerHand.push_back(ezBlk[deckIt]);
                        deckIt++;
                        dealerIt++;
                        dealerVal+=getCardVal(dealerHand[dealerIt-1]);
                    }
                    cout<<"Dealer cards: ";
                    for(int i=0;i<dealerIt;i++){
                        cout<<dealerHand[i]<<",";

                    }
                    cout <<endl
                            <<"Dealer card total:"<<dealerVal<<endl;
                    cout<<string(3,'\n');
                    cout<<"Your cards: ";
                    for(int i=0;i<playerIt;i++){
                        cout<<playerHand[i]<<","<<endl;
                    }
                    cout<<"Your cards value:"<<playerVal<<endl;
                    if(dealerVal>21){
                        cout<<"Dealer busts!"<<endl;
                        playerTotal+=playerBet;
                        cout<<"You won:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;    
                    }
                    else if(dealerVal==21){
                        cout<<"Dealer blackjack! Better luck next time!"<<endl;
                        playerTotal-=playerBet;
                        cout<<"You lost:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    else if(dealerVal>playerVal){
                        cout<<"Dealer wins! Better luck next time!"<<endl;
                        playerTotal-=playerBet;
                        cout<<"You lost:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    else if(playerVal>dealerVal){
                        cout<<"You win!"<<endl;
                        playerTotal+=playerBet;
                        cout<<"You won:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    
                }
                
                cout<<"Would you like to play again? Y for yes, "
                        "anything else for no"<<endl;
                cin >> play;
            
            
            
            
            }
            break;
        }
        case '2':
        {
            int deckIt=0;  
            vector<string> ezBlk;
            for(int i=0;i<7;i++){
                createDeck(ezBlk);
            }
            cout << "Shuffling cards..."<<endl;
            random_shuffle(ezBlk.begin(),ezBlk.end());
            Sleep(3000);
            char play='y';
            while(play=='y'||play=='Y'){
                int playerIt=0; //player iterator for cards
                int dealerIt=0; //dealer iterator for cards
                int playerBet;
                vector<string> playerHand;
                vector<string> dealerHand;
                cout << "Welcome to the hard mode!" << endl
                         << endl
                        << "Please place your bet!"<<endl;
                cout << "Your total:$"<< playerTotal << endl;
                cin>>playerBet;
                while(playerBet>playerTotal){
                    cout << "You do not have enough funds!"<< endl;
                    cout << "Enter new total:";
                    cin>>playerBet;
                }
                cout << "$"<<playerBet<<" is on the table!"<<endl;
                cout << string(50, '\n');
                playerHand.push_back(ezBlk[deckIt]);
                deckIt++;
                playerIt++;
                dealerHand.push_back(ezBlk[deckIt]);
                dealerIt++;
                deckIt++;
                playerHand.push_back(ezBlk[deckIt]);
                deckIt++;
                playerIt++;
                dealerHand.push_back(ezBlk[deckIt]);
                dealerIt++;
                deckIt++;
                int dealerVal=getCardVal(dealerHand[0])
                    +getCardVal(dealerHand[1]);
                int playerVal=getCardVal(playerHand[0])
                    +getCardVal(playerHand[1]);
                cout<<"Dealer's hand:" << endl
                        << dealerHand[0] << endl;
                cout << string(3,'\n');
                cout << "Your hand:"<< endl
                        << playerHand[0] << endl << playerHand[1] << endl
                        << "Your hand value:" << playerVal << endl;
                
                
                cout << "What you like to hit(h) or stand(s)?";
                string temp;
                cin>>temp;
                while(temp[0]!='h'&&temp[0]!='s'){
                    cout << endl << "Invalid option! Please enter H or S:";
                    cin.clear();
                    cin>>temp;
                }
                if(temp[0]=='h'||temp[0]=='H'){
                    while(temp[0]=='h'||temp[0]=='H'){
                        playerHand.push_back(ezBlk[deckIt]);
                        playerIt++;
                        deckIt++;
                        cout<<"Your card is " << playerHand[playerIt-1] <<endl;
                        playerVal+=getCardVal(playerHand[playerIt-1]);
                        cout<<"Your cards value is:"<<playerVal<<endl;
                        cout<<"Your cards are:";
                        
                        
                        for(int i=0;i<playerIt;i++){
                            cout <<playerHand[i]<<",";
                        }
                        
                        if(playerVal==21){
                            cout <<endl<<"Blackjack!"<< endl;
                            playerTotal+=(playerBet*2);
                            break;
                        }
                        if(playerVal>21){
                            cout <<endl<< "You bust!"<<endl;
                            playerTotal=playerTotal-playerBet;
                            cout<<"You lost:$"<<playerBet<<endl
                                    <<"Your total is:$"<<playerTotal<<endl;
                            break;
                        }
                        if(playerVal<21){
                            cout <<endl <<"Would you like to hit[h] "
                                    "or stand[s]?";
                            cin>>temp[0];
                        }
                        
                    }
                }
                if(temp[0]=='s'||temp[0]=='S'){
                    cout << string(50, '\n');
                    cout << "You chose to stand!"<<endl;
                    while(dealerVal<=17){
                        dealerHand.push_back(ezBlk[deckIt]);
                        deckIt++;
                        dealerIt++;
                        dealerVal+=getCardVal(dealerHand[dealerIt-1]);
                    }
                    cout<<"Dealer cards: ";
                    for(int i=0;i<dealerIt;i++){
                        cout<<dealerHand[i]<<",";

                    }
                    cout <<endl
                            <<"Dealer card total:"<<dealerVal<<endl;
                    cout<<string(3,'\n');
                    cout<<"Your cards: ";
                    for(int i=0;i<playerIt;i++){
                        cout<<playerHand[i]<<","<<endl;
                    }
                    cout<<"Your cards value:"<<playerVal<<endl;
                    if(dealerVal>21){
                        cout<<"Dealer busts!"<<endl;
                        playerTotal+=playerBet;
                        cout<<"You won:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;    
                    }
                    else if(dealerVal==21){
                        cout<<"Dealer blackjack! Better luck next time!"<<endl;
                        playerTotal-=playerBet;
                        cout<<"You lost:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    else if(dealerVal>playerVal){
                        cout<<"Dealer wins! Better luck next time!"<<endl;
                        playerTotal-=playerBet;
                        cout<<"You lost:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    else if(playerVal>dealerVal){
                        cout<<"You win!"<<endl;
                        playerTotal+=playerBet;
                        cout<<"You won:$"<<playerBet<<endl
                                <<"Your new total is:$"<<playerTotal<<endl;
                    }
                    
                }
                
                cout<<"Would you like to play again? Y for yes, "
                        "anything else for no"<<endl;
                cin >> play;
            
            
            
            
            }
            break;
        }
        case '3':
        {
            cout << "Welcome to the tutorial. Blackjack is a" <<endl<<
                    "very simple game to understand, but offers "<<endl<<
                    "more complex strategies to master."<<endl;
            cout<<"The objective in blackjack is to have a higher"<<endl<<
                    "card value than the dealer at the end of the"<<endl<<
                    "round. The dealer deals cards from left to right"<<endl<<
                    "giving each player one card, then"
                    " giving themselves"<<endl<<
                    "a card last. The dealer goes around again giving"<<endl<<
                    "players another card, then gives"
                    " themselves a card"<<endl<<
                    "that is faced down. Starting from left to right"<<endl<<
                    "again the dealer goes around and gives the "
                    "player a chance"<<endl<<
                    "to hit (take another card)or stand "
                    "(keep your current" <<endl<<
                    "cards only). After every player gets a turn,"
                    " the dealer" <<endl<<
                    "does their hit/stands. A dealer stands "
                    "if they have "<<endl<<
                    "card values equal to or higher than 17 "
                    "(the hard mode"<<endl<<
                    "in this game). In the easy mode in"
                    " this game the dealer"<<endl<<
                    "stands at 19, which offers a higher "
                    "chance to bust for"<<endl<<
                    "the dealer. If a player or the dealer"
                    " goes over 21,"<<endl<<
                    "they bust and automatically lose. If "
                    "the dealer hits"<<endl<<
                    "a blackjack (21) then everyone loses,"
                    " unless the player"<<endl<<
                    "also has a blackjack."<<endl <<"The player wins if they "
                    "have a higher card value"<<endl<<"at the end of the round"
                    <<endl;
            cout <<"This is an example game" <<endl<<endl;
            vector<string> dealerHand;
            dealerHand.push_back("King of Hearts");
            dealerHand.push_back("7 of Spades");
            vector<string> playerHand;
            playerHand.push_back("King of Spades");
            playerHand.push_back("3 of Clubs");
            int playerVal=getCardVal(playerHand[0]+playerHand[1]);
            int dealerVal=getCardVal(dealerHand[0]+dealerHand[1]);
            for(int i=0;i<1;i++){
                cout<<dealerHand[i]<<",";
            }
            cout<<endl<<"Dealer Total:"<<dealerVal<<endl<<endl<<endl;
            for(int i=0;i<2;i++){
                cout<<dealerHand[i]<<",";
            }
            cout<<endl<<playerVal<<endl;
            cout<<"The player has a total of "<<playerVal<<". So they should"
                    "probably hit!"<<endl <<endl<<endl;
            playerHand.push_back("7 of Diamonds");
            playerVal+=getCardVal("7 of Diamonds");
            cout<<"The player got a 7 of Diamonds after the hit!"
                    "The dealer will stand because their at 17!"<<endl;
            dealerVal+=getCardVal(dealerHand[1]);
            for(int i=0;i<2;i++){
                cout<<dealerHand[i]<<",";
            }
            cout<<endl<<"Dealer Total:"<<dealerVal<<endl<<endl<<endl;
            for(int i=0;i<2;i++){
                cout<<playerHand[i]<<",";
            }
            cout<<endl<<playerVal<<endl;
            cout<<"The player will win after the hit because they have 21!"
                    <<endl;
            
            cout<<endl<<"Enter anything to quit"<<endl;
            char el;
            cin>>el;
            
            
            break;
        }
        case '4':
        {
            cout <<"Ending..."<<endl;
            break;
        }
        default:
        {
            cout <<"Invalid option!"<<endl;
            cin.clear();
            menu();
            break;
            
                    
            
        }
    }
}

